/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    subghz_phy_app.c
  * @author  MCD Application Team
  * @brief   Application of the SubGHz_Phy Middleware
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "platform.h"
#include "sys_app.h"
#include "subghz_phy_app.h"
#include "radio.h"

/* USER CODE BEGIN Includes */
#include "stm32_timer.h"
#include "stm32_seq.h"
#include "utilities_def.h"
#include "stdint.h"
#include "stdbool.h"

#define MAX_APP_BUFFER_SIZE          255
#define RX_TIMEOUT_VALUE              3000
#define TX_TIMEOUT_VALUE              3000


/* USER CODE END Includes */

/* External variables ---------------------------------------------------------*/
/* USER CODE BEGIN EV */

	uint32_t crctest;
uint8_t uartPackage[64];

uint8_t enCrypt[200];
uint8_t denCrypt[200];

uint8_t uartPackageCout = 0;
uint32_t crcPackage;
uint16_t randomCode;
uint8_t radioPackage[50];

uint32_t loraPassword[8];

uint8_t Rx_Hand_Shake[2];
uint8_t Tx_Hand_Shake[2] = {0xF1,0};

uint8_t ID[2] = {1,2};
uint8_t Lora_Now_ID;
uint8_t Connected[3] = {0,0,0};
uint8_t Size[3] = {0,0,0};
uint8_t Count[3][20];
uint8_t LoraPacket[100];
uint8_t DATA[3][20][20];
uint8_t test[3] = {9,9,9};
/* USER CODE END EV */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
static void SerialProcess(void);
static void Timeout_HS(void);
static void CheckNext(void);
static void HandShakeMode();
static int check_CRC(void);
static void Check_Connect();
static void Serial_ISR(uint8_t *PData, uint16_t Size, uint8_t Error);
extern CRC_HandleTypeDef hcrc;
/* USER CODE END PM */
/* Private variables ---------------------------------------------------------*/
/* Radio events function pointer */
static RadioEvents_t RadioEvents;
/* USER CODE BEGIN PV */
static UTIL_TIMER_Object_t Eventtimeout;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/*!
 * @brief Function to be executed on Radio Tx Done event
 */
static void OnTxDone(void);

/**
  * @brief Function to be executed on Radio Rx Done event
  * @param  payload ptr of buffer received
  * @param  size buffer size
  * @param  rssi
  * @param  LoraSnr_FskCfo
  */
static void OnRxDone(uint8_t *payload, uint16_t size, int16_t rssi, int8_t LoraSnr_FskCfo);

/**
  * @brief Function executed on Radio Tx Timeout event
  */
static void OnTxTimeout(void);

/**
  * @brief Function executed on Radio Rx Timeout event
  */
static void OnRxTimeout(void);

/**
  * @brief Function executed on Radio Rx Error event
  */
static void OnRxError(void);

/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Exported functions ---------------------------------------------------------*/
void SubghzApp_Init(void)
{
  /* USER CODE BEGIN SubghzApp_Init_1 */
	UTIL_ADV_TRACE_Init();
	UTIL_TIMER_Create(&Eventtimeout,1500, UTIL_TIMER_PERIODIC, Timeout_HS, NULL);

	UTIL_ADV_TRACE_StartRxProcess(Serial_ISR);
  /* USER CODE END SubghzApp_Init_1 */

  /* Radio initialization */
  RadioEvents.TxDone = OnTxDone;
  RadioEvents.RxDone = OnRxDone;
  RadioEvents.TxTimeout = OnTxTimeout;
  RadioEvents.RxTimeout = OnRxTimeout;
  RadioEvents.RxError = OnRxError;

  Radio.Init(&RadioEvents);

  /* USER CODE BEGIN SubghzApp_Init_2 */
  Radio.SetChannel(RF_FREQUENCY);

  	Radio.SetTxConfig(MODEM_LORA, TX_OUTPUT_POWER, 0, LORA_BANDWIDTH,
  			  LORA_SPREADING_FACTOR, LORA_CODINGRATE,
  			  LORA_PREAMBLE_LENGTH, LORA_FIX_LENGTH_PAYLOAD_ON,
  			  true, 0, 0, LORA_IQ_INVERSION_ON, TX_TIMEOUT_VALUE);

  	Radio.SetRxConfig(MODEM_LORA, LORA_BANDWIDTH, LORA_SPREADING_FACTOR,
  			  LORA_CODINGRATE, 0, LORA_PREAMBLE_LENGTH,
  			  LORA_SYMBOL_TIMEOUT, LORA_FIX_LENGTH_PAYLOAD_ON,
  			  0, true, 0, 0, LORA_IQ_INVERSION_ON, true);

  	Radio.SetMaxPayloadLength(MODEM_LORA, MAX_APP_BUFFER_SIZE);

  	//Radio.RxBoosted(0);
  	UTIL_SEQ_RegTask((1<<CFG_SEQ_Task_Vcom), UTIL_SEQ_RFU, SerialProcess);
  	Lora_Now_ID = ID[0];
  	Check_Connect(Lora_Now_ID);
  /* USER CODE END SubghzApp_Init_2 */
}

/* USER CODE BEGIN EF */
static void Check_Connect()
{
	if(Lora_Now_ID == Connected[Lora_Now_ID])
	{
		if(Size[Lora_Now_ID] > 0)
		{
			//get the first packet
			for(int i=0;i<Count[Lora_Now_ID][0];i++)
				LoraPacket[i] = DATA[Lora_Now_ID][0][i];
			//clear the first packet
			for(int i=0;i<Count[Lora_Now_ID][0];i++)
				DATA[Lora_Now_ID][0][i] = 0;
			//arrange array DATA
			for(int i=1;i<20;i++)
			{
				for(int j=0;j<20;j++)
				{
					DATA[Lora_Now_ID][i-1][j] = DATA[Lora_Now_ID][i][j];
				}
			}
			//decrease size and delete count
			Size[Lora_Now_ID]--;
			for(int i=0;i<20-1;i++)
			{
				Count[Lora_Now_ID][i] = Count[Lora_Now_ID][i+1];
			}

			Radio.Send(LoraPacket,Count[Lora_Now_ID][0]);

			for(int i =0;i<100;i++)
				LoraPacket[i] = 0;

		}
	}
	else
	{
		HandShakeMode(Lora_Now_ID);
	}
}

static void HandShakeMode()
{
	Tx_Hand_Shake[1] = Lora_Now_ID;
	Radio.Send(Tx_Hand_Shake,2);
}

static void SerialProcess(void)
{
		if(check_CRC())
		{
			ID_UARTPacket = uartPackage[1];
			Count[ID_UARTPacket][Size[ID_UARTPacket]] = uartPackageCout - 5;
			for(int i=0;i<Count[ID_UARTPacket][Size[ID_UARTPacket]];i++)
			{
				DATA[ID_UARTPacket][Size[ID_UARTPacket]][i] = uartPackage[i+1];
			}
			Size[ID_UARTPacket]++;
		}
	uartPackageCout = 0;
	for(int i=0;i<64;i++)
		uartPackage[i] = 0;

}

static int check_CRC(void)
{
	for (int i = 0; i < 4; i++)
	{
		crcPackage = (crcPackage<<8)|uartPackage[uartPackageCout-4+i];
	}
	if(crcPackage == ~HAL_CRC_Calculate(&hcrc, (uint32_t*)uartPackage, (uartPackageCout-4)))
	{
		return 1;
	}
	return 0;

}

static void Timeout_HS(void)
{
	UTIL_TIMER_Stop(&Eventtimeout);
	Connected[Lora_Now_ID] = 0;
	CheckNext();

}
/* USER CODE END EF */

/* Private functions ---------------------------------------------------------*/
static void OnTxDone(void)
{
  /* USER CODE BEGIN OnTxDone */
	UTIL_TIMER_Start(&Eventtimeout);
	Radio.RxBoosted(0);

  /* USER CODE END OnTxDone */
}

static void OnRxDone(uint8_t *payload, uint16_t size, int16_t rssi, int8_t LoraSnr_FskCfo)
{
  /* USER CODE BEGIN OnRxDone */
	for(int i=0;i<size;i++)
		LoraPacket[i] = payload[i];
	if(LoraPacket[1]==Lora_Now_ID)
	{
		UTIL_TIMER_Stop(&Eventtimeout);
		switch(LoraPacket[0])
		{
		case 0xF0:
			Connected[Lora_Now_ID] = Lora_Now_ID;
			CheckNext();
			break;
		case 0xA0: // byte Sync ok
			for(int i = 0;i<size-2;i++)
			{
				LoraPacket[i] = LoraPacket[i+2];
			}
			vcom_Trace(LoraPacket, size-2);
			break;
		case 0xE0:// byte Sync error

			break;
		default :

			break;
		}
	}
	else
	{

	}

	for(int i=0;i<size;i++)
		LoraPacket[i] = 0;

}

static void CheckNext(void)
{
	Lora_Now_ID++;
	if(Lora_Now_ID >2)
	{
		Lora_Now_ID = 1;
	}
	Check_Connect();
}

static void OnTxTimeout(void)
{
  /* USER CODE BEGIN OnTxTimeout */
	Radio.RxBoosted(0);
  /* USER CODE END OnTxTimeout */
}

static void OnRxTimeout(void)
{
  /* USER CODE BEGIN OnRxTimeout */
	HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_10);
	Radio.RxBoosted(0);
  /* USER CODE END OnRxTimeout */
}

static void OnRxError(void)
{
  /* USER CODE BEGIN OnRxError */
	Radio.RxBoosted(0);
  /* USER CODE END OnRxError */
}

/* USER CODE BEGIN PrFD */
static void Serial_ISR(uint8_t *PData, uint16_t Size, uint8_t Error)
{
	UTIL_TIMER_Stop(&Eventtimeout);
	UTIL_TIMER_Start(&Eventtimeout);
	uartPackageCout++;
	uartPackage[uartPackageCout-1] = *PData;

	if (uartPackageCout >= 14) {

		UTIL_TIMER_Stop(&Eventtimeout);
		UTIL_SEQ_SetTask((1 << CFG_SEQ_Task_Vcom), CFG_SEQ_Prio_0);
	}
}
/* USER CODE END PrFD */
